/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGradleAppTestModule } from '../../../test.module';
import { ArepitaDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/arepita/arepita-delete-dialog.component';
import { ArepitaService } from '../../../../../../main/webapp/app/entities/arepita/arepita.service';

describe('Component Tests', () => {

    describe('Arepita Management Delete Component', () => {
        let comp: ArepitaDeleteDialogComponent;
        let fixture: ComponentFixture<ArepitaDeleteDialogComponent>;
        let service: ArepitaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGradleAppTestModule],
                declarations: [ArepitaDeleteDialogComponent],
                providers: [
                    ArepitaService
                ]
            })
            .overrideTemplate(ArepitaDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ArepitaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ArepitaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
