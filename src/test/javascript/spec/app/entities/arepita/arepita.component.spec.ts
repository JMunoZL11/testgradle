/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGradleAppTestModule } from '../../../test.module';
import { ArepitaComponent } from '../../../../../../main/webapp/app/entities/arepita/arepita.component';
import { ArepitaService } from '../../../../../../main/webapp/app/entities/arepita/arepita.service';
import { Arepita } from '../../../../../../main/webapp/app/entities/arepita/arepita.model';

describe('Component Tests', () => {

    describe('Arepita Management Component', () => {
        let comp: ArepitaComponent;
        let fixture: ComponentFixture<ArepitaComponent>;
        let service: ArepitaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGradleAppTestModule],
                declarations: [ArepitaComponent],
                providers: [
                    ArepitaService
                ]
            })
            .overrideTemplate(ArepitaComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ArepitaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ArepitaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Arepita(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.arepitas[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
