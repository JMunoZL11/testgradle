/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGradleAppTestModule } from '../../../test.module';
import { ArepitaDetailComponent } from '../../../../../../main/webapp/app/entities/arepita/arepita-detail.component';
import { ArepitaService } from '../../../../../../main/webapp/app/entities/arepita/arepita.service';
import { Arepita } from '../../../../../../main/webapp/app/entities/arepita/arepita.model';

describe('Component Tests', () => {

    describe('Arepita Management Detail Component', () => {
        let comp: ArepitaDetailComponent;
        let fixture: ComponentFixture<ArepitaDetailComponent>;
        let service: ArepitaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGradleAppTestModule],
                declarations: [ArepitaDetailComponent],
                providers: [
                    ArepitaService
                ]
            })
            .overrideTemplate(ArepitaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ArepitaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ArepitaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Arepita(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.arepita).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
