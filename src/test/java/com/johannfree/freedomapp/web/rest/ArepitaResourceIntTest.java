package com.johannfree.freedomapp.web.rest;

import com.johannfree.freedomapp.TestGradleApp;

import com.johannfree.freedomapp.domain.Arepita;
import com.johannfree.freedomapp.repository.ArepitaRepository;
import com.johannfree.freedomapp.service.ArepitaService;
import com.johannfree.freedomapp.service.dto.ArepitaDTO;
import com.johannfree.freedomapp.service.mapper.ArepitaMapper;
import com.johannfree.freedomapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static com.johannfree.freedomapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ArepitaResource REST controller.
 *
 * @see ArepitaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGradleApp.class)
public class ArepitaResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final String DEFAULT_RECETA = "AAAAAAAAAA";
    private static final String UPDATED_RECETA = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGEN = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGEN = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGEN_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGEN_CONTENT_TYPE = "image/png";

    @Autowired
    private ArepitaRepository arepitaRepository;

    @Autowired
    private ArepitaMapper arepitaMapper;

    @Autowired
    private ArepitaService arepitaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restArepitaMockMvc;

    private Arepita arepita;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ArepitaResource arepitaResource = new ArepitaResource(arepitaService);
        this.restArepitaMockMvc = MockMvcBuilders.standaloneSetup(arepitaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Arepita createEntity(EntityManager em) {
        Arepita arepita = new Arepita()
            .nombre(DEFAULT_NOMBRE)
            .descripcion(DEFAULT_DESCRIPCION)
            .receta(DEFAULT_RECETA)
            .imagen(DEFAULT_IMAGEN)
            .imagenContentType(DEFAULT_IMAGEN_CONTENT_TYPE);
        return arepita;
    }

    @Before
    public void initTest() {
        arepita = createEntity(em);
    }

    @Test
    @Transactional
    public void createArepita() throws Exception {
        int databaseSizeBeforeCreate = arepitaRepository.findAll().size();

        // Create the Arepita
        ArepitaDTO arepitaDTO = arepitaMapper.toDto(arepita);
        restArepitaMockMvc.perform(post("/api/arepitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arepitaDTO)))
            .andExpect(status().isCreated());

        // Validate the Arepita in the database
        List<Arepita> arepitaList = arepitaRepository.findAll();
        assertThat(arepitaList).hasSize(databaseSizeBeforeCreate + 1);
        Arepita testArepita = arepitaList.get(arepitaList.size() - 1);
        assertThat(testArepita.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testArepita.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testArepita.getReceta()).isEqualTo(DEFAULT_RECETA);
        assertThat(testArepita.getImagen()).isEqualTo(DEFAULT_IMAGEN);
        assertThat(testArepita.getImagenContentType()).isEqualTo(DEFAULT_IMAGEN_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createArepitaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = arepitaRepository.findAll().size();

        // Create the Arepita with an existing ID
        arepita.setId(1L);
        ArepitaDTO arepitaDTO = arepitaMapper.toDto(arepita);

        // An entity with an existing ID cannot be created, so this API call must fail
        restArepitaMockMvc.perform(post("/api/arepitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arepitaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Arepita in the database
        List<Arepita> arepitaList = arepitaRepository.findAll();
        assertThat(arepitaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllArepitas() throws Exception {
        // Initialize the database
        arepitaRepository.saveAndFlush(arepita);

        // Get all the arepitaList
        restArepitaMockMvc.perform(get("/api/arepitas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(arepita.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
            .andExpect(jsonPath("$.[*].receta").value(hasItem(DEFAULT_RECETA.toString())))
            .andExpect(jsonPath("$.[*].imagenContentType").value(hasItem(DEFAULT_IMAGEN_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imagen").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGEN))));
    }

    @Test
    @Transactional
    public void getArepita() throws Exception {
        // Initialize the database
        arepitaRepository.saveAndFlush(arepita);

        // Get the arepita
        restArepitaMockMvc.perform(get("/api/arepitas/{id}", arepita.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(arepita.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.receta").value(DEFAULT_RECETA.toString()))
            .andExpect(jsonPath("$.imagenContentType").value(DEFAULT_IMAGEN_CONTENT_TYPE))
            .andExpect(jsonPath("$.imagen").value(Base64Utils.encodeToString(DEFAULT_IMAGEN)));
    }

    @Test
    @Transactional
    public void getNonExistingArepita() throws Exception {
        // Get the arepita
        restArepitaMockMvc.perform(get("/api/arepitas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArepita() throws Exception {
        // Initialize the database
        arepitaRepository.saveAndFlush(arepita);
        int databaseSizeBeforeUpdate = arepitaRepository.findAll().size();

        // Update the arepita
        Arepita updatedArepita = arepitaRepository.findOne(arepita.getId());
        // Disconnect from session so that the updates on updatedArepita are not directly saved in db
        em.detach(updatedArepita);
        updatedArepita
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .receta(UPDATED_RECETA)
            .imagen(UPDATED_IMAGEN)
            .imagenContentType(UPDATED_IMAGEN_CONTENT_TYPE);
        ArepitaDTO arepitaDTO = arepitaMapper.toDto(updatedArepita);

        restArepitaMockMvc.perform(put("/api/arepitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arepitaDTO)))
            .andExpect(status().isOk());

        // Validate the Arepita in the database
        List<Arepita> arepitaList = arepitaRepository.findAll();
        assertThat(arepitaList).hasSize(databaseSizeBeforeUpdate);
        Arepita testArepita = arepitaList.get(arepitaList.size() - 1);
        assertThat(testArepita.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testArepita.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testArepita.getReceta()).isEqualTo(UPDATED_RECETA);
        assertThat(testArepita.getImagen()).isEqualTo(UPDATED_IMAGEN);
        assertThat(testArepita.getImagenContentType()).isEqualTo(UPDATED_IMAGEN_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingArepita() throws Exception {
        int databaseSizeBeforeUpdate = arepitaRepository.findAll().size();

        // Create the Arepita
        ArepitaDTO arepitaDTO = arepitaMapper.toDto(arepita);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restArepitaMockMvc.perform(put("/api/arepitas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arepitaDTO)))
            .andExpect(status().isCreated());

        // Validate the Arepita in the database
        List<Arepita> arepitaList = arepitaRepository.findAll();
        assertThat(arepitaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteArepita() throws Exception {
        // Initialize the database
        arepitaRepository.saveAndFlush(arepita);
        int databaseSizeBeforeDelete = arepitaRepository.findAll().size();

        // Get the arepita
        restArepitaMockMvc.perform(delete("/api/arepitas/{id}", arepita.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Arepita> arepitaList = arepitaRepository.findAll();
        assertThat(arepitaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Arepita.class);
        Arepita arepita1 = new Arepita();
        arepita1.setId(1L);
        Arepita arepita2 = new Arepita();
        arepita2.setId(arepita1.getId());
        assertThat(arepita1).isEqualTo(arepita2);
        arepita2.setId(2L);
        assertThat(arepita1).isNotEqualTo(arepita2);
        arepita1.setId(null);
        assertThat(arepita1).isNotEqualTo(arepita2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ArepitaDTO.class);
        ArepitaDTO arepitaDTO1 = new ArepitaDTO();
        arepitaDTO1.setId(1L);
        ArepitaDTO arepitaDTO2 = new ArepitaDTO();
        assertThat(arepitaDTO1).isNotEqualTo(arepitaDTO2);
        arepitaDTO2.setId(arepitaDTO1.getId());
        assertThat(arepitaDTO1).isEqualTo(arepitaDTO2);
        arepitaDTO2.setId(2L);
        assertThat(arepitaDTO1).isNotEqualTo(arepitaDTO2);
        arepitaDTO1.setId(null);
        assertThat(arepitaDTO1).isNotEqualTo(arepitaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(arepitaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(arepitaMapper.fromId(null)).isNull();
    }
}
