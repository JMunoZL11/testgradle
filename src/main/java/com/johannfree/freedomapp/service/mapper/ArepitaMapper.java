package com.johannfree.freedomapp.service.mapper;

import com.johannfree.freedomapp.domain.*;
import com.johannfree.freedomapp.service.dto.ArepitaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Arepita and its DTO ArepitaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ArepitaMapper extends EntityMapper<ArepitaDTO, Arepita> {



    default Arepita fromId(Long id) {
        if (id == null) {
            return null;
        }
        Arepita arepita = new Arepita();
        arepita.setId(id);
        return arepita;
    }
}
