package com.johannfree.freedomapp.service.impl;

import com.johannfree.freedomapp.service.ArepitaService;
import com.johannfree.freedomapp.domain.Arepita;
import com.johannfree.freedomapp.repository.ArepitaRepository;
import com.johannfree.freedomapp.service.dto.ArepitaDTO;
import com.johannfree.freedomapp.service.mapper.ArepitaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Arepita.
 */
@Service
@Transactional
public class ArepitaServiceImpl implements ArepitaService {

    private final Logger log = LoggerFactory.getLogger(ArepitaServiceImpl.class);

    private final ArepitaRepository arepitaRepository;

    private final ArepitaMapper arepitaMapper;

    public ArepitaServiceImpl(ArepitaRepository arepitaRepository, ArepitaMapper arepitaMapper) {
        this.arepitaRepository = arepitaRepository;
        this.arepitaMapper = arepitaMapper;
    }

    /**
     * Save a arepita.
     *
     * @param arepitaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ArepitaDTO save(ArepitaDTO arepitaDTO) {
        log.debug("Request to save Arepita : {}", arepitaDTO);
        Arepita arepita = arepitaMapper.toEntity(arepitaDTO);
        arepita = arepitaRepository.save(arepita);
        return arepitaMapper.toDto(arepita);
    }

    /**
     * Get all the arepitas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ArepitaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Arepitas");
        return arepitaRepository.findAll(pageable)
            .map(arepitaMapper::toDto);
    }

    /**
     * Get one arepita by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ArepitaDTO findOne(Long id) {
        log.debug("Request to get Arepita : {}", id);
        Arepita arepita = arepitaRepository.findOne(id);
        return arepitaMapper.toDto(arepita);
    }

    /**
     * Delete the arepita by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Arepita : {}", id);
        arepitaRepository.delete(id);
    }
}
