package com.johannfree.freedomapp.service;

import com.johannfree.freedomapp.service.dto.ArepitaDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Arepita.
 */
public interface ArepitaService {

    /**
     * Save a arepita.
     *
     * @param arepitaDTO the entity to save
     * @return the persisted entity
     */
    ArepitaDTO save(ArepitaDTO arepitaDTO);

    /**
     * Get all the arepitas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ArepitaDTO> findAll(Pageable pageable);

    /**
     * Get the "id" arepita.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ArepitaDTO findOne(Long id);

    /**
     * Delete the "id" arepita.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
