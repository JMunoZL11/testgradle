package com.johannfree.freedomapp.repository;

import com.johannfree.freedomapp.domain.Arepita;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Arepita entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArepitaRepository extends JpaRepository<Arepita, Long> {

}
