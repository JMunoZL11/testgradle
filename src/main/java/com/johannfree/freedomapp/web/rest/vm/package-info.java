/**
 * View Models used by Spring MVC REST controllers.
 */
package com.johannfree.freedomapp.web.rest.vm;
