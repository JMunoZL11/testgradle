package com.johannfree.freedomapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.johannfree.freedomapp.service.ArepitaService;
import com.johannfree.freedomapp.web.rest.errors.BadRequestAlertException;
import com.johannfree.freedomapp.web.rest.util.HeaderUtil;
import com.johannfree.freedomapp.web.rest.util.PaginationUtil;
import com.johannfree.freedomapp.service.dto.ArepitaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Arepita.
 */
@RestController
@RequestMapping("/api")
public class ArepitaResource {

    private final Logger log = LoggerFactory.getLogger(ArepitaResource.class);

    private static final String ENTITY_NAME = "arepita";

    private final ArepitaService arepitaService;

    public ArepitaResource(ArepitaService arepitaService) {
        this.arepitaService = arepitaService;
    }

    /**
     * POST  /arepitas : Create a new arepita.
     *
     * @param arepitaDTO the arepitaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new arepitaDTO, or with status 400 (Bad Request) if the arepita has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/arepitas")
    @Timed
    public ResponseEntity<ArepitaDTO> createArepita(@RequestBody ArepitaDTO arepitaDTO) throws URISyntaxException {
        log.debug("REST request to save Arepita : {}", arepitaDTO);
        if (arepitaDTO.getId() != null) {
            throw new BadRequestAlertException("A new arepita cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ArepitaDTO result = arepitaService.save(arepitaDTO);
        return ResponseEntity.created(new URI("/api/arepitas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /arepitas : Updates an existing arepita.
     *
     * @param arepitaDTO the arepitaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated arepitaDTO,
     * or with status 400 (Bad Request) if the arepitaDTO is not valid,
     * or with status 500 (Internal Server Error) if the arepitaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/arepitas")
    @Timed
    public ResponseEntity<ArepitaDTO> updateArepita(@RequestBody ArepitaDTO arepitaDTO) throws URISyntaxException {
        log.debug("REST request to update Arepita : {}", arepitaDTO);
        if (arepitaDTO.getId() == null) {
            return createArepita(arepitaDTO);
        }
        ArepitaDTO result = arepitaService.save(arepitaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, arepitaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /arepitas : get all the arepitas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of arepitas in body
     */
    @GetMapping("/arepitas")
    @Timed
    public ResponseEntity<List<ArepitaDTO>> getAllArepitas(Pageable pageable) {
        log.debug("REST request to get a page of Arepitas");
        Page<ArepitaDTO> page = arepitaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/arepitas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /arepitas/:id : get the "id" arepita.
     *
     * @param id the id of the arepitaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the arepitaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/arepitas/{id}")
    @Timed
    public ResponseEntity<ArepitaDTO> getArepita(@PathVariable Long id) {
        log.debug("REST request to get Arepita : {}", id);
        ArepitaDTO arepitaDTO = arepitaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(arepitaDTO));
    }

    /**
     * DELETE  /arepitas/:id : delete the "id" arepita.
     *
     * @param id the id of the arepitaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/arepitas/{id}")
    @Timed
    public ResponseEntity<Void> deleteArepita(@PathVariable Long id) {
        log.debug("REST request to delete Arepita : {}", id);
        arepitaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
