import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TestGradleAppArepitaModule } from './arepita/arepita.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TestGradleAppArepitaModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGradleAppEntityModule {}
