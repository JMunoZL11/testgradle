import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Arepita } from './arepita.model';
import { ArepitaPopupService } from './arepita-popup.service';
import { ArepitaService } from './arepita.service';

@Component({
    selector: 'jhi-arepita-dialog',
    templateUrl: './arepita-dialog.component.html'
})
export class ArepitaDialogComponent implements OnInit {

    arepita: Arepita;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private arepitaService: ArepitaService,
        private elementRef: ElementRef,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.arepita, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.arepita.id !== undefined) {
            this.subscribeToSaveResponse(
                this.arepitaService.update(this.arepita));
        } else {
            this.subscribeToSaveResponse(
                this.arepitaService.create(this.arepita));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Arepita>>) {
        result.subscribe((res: HttpResponse<Arepita>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Arepita) {
        this.eventManager.broadcast({ name: 'arepitaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-arepita-popup',
    template: ''
})
export class ArepitaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private arepitaPopupService: ArepitaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.arepitaPopupService
                    .open(ArepitaDialogComponent as Component, params['id']);
            } else {
                this.arepitaPopupService
                    .open(ArepitaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
