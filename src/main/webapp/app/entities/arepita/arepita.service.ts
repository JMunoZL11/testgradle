import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Arepita } from './arepita.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Arepita>;

@Injectable()
export class ArepitaService {

    private resourceUrl =  SERVER_API_URL + 'api/arepitas';

    constructor(private http: HttpClient) { }

    create(arepita: Arepita): Observable<EntityResponseType> {
        const copy = this.convert(arepita);
        return this.http.post<Arepita>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(arepita: Arepita): Observable<EntityResponseType> {
        const copy = this.convert(arepita);
        return this.http.put<Arepita>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Arepita>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Arepita[]>> {
        const options = createRequestOption(req);
        return this.http.get<Arepita[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Arepita[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Arepita = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Arepita[]>): HttpResponse<Arepita[]> {
        const jsonResponse: Arepita[] = res.body;
        const body: Arepita[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Arepita.
     */
    private convertItemFromServer(arepita: Arepita): Arepita {
        const copy: Arepita = Object.assign({}, arepita);
        return copy;
    }

    /**
     * Convert a Arepita to a JSON which can be sent to the server.
     */
    private convert(arepita: Arepita): Arepita {
        const copy: Arepita = Object.assign({}, arepita);
        return copy;
    }
}
