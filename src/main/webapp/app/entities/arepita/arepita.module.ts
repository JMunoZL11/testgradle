import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGradleAppSharedModule } from '../../shared';
import {
    ArepitaService,
    ArepitaPopupService,
    ArepitaComponent,
    ArepitaDetailComponent,
    ArepitaDialogComponent,
    ArepitaPopupComponent,
    ArepitaDeletePopupComponent,
    ArepitaDeleteDialogComponent,
    arepitaRoute,
    arepitaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...arepitaRoute,
    ...arepitaPopupRoute,
];

@NgModule({
    imports: [
        TestGradleAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ArepitaComponent,
        ArepitaDetailComponent,
        ArepitaDialogComponent,
        ArepitaDeleteDialogComponent,
        ArepitaPopupComponent,
        ArepitaDeletePopupComponent,
    ],
    entryComponents: [
        ArepitaComponent,
        ArepitaDialogComponent,
        ArepitaPopupComponent,
        ArepitaDeleteDialogComponent,
        ArepitaDeletePopupComponent,
    ],
    providers: [
        ArepitaService,
        ArepitaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGradleAppArepitaModule {}
