import { BaseEntity } from './../../shared';

export class Arepita implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public descripcion?: string,
        public receta?: string,
        public imagenContentType?: string,
        public imagen?: any,
    ) {
    }
}
