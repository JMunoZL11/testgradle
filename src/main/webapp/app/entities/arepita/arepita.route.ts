import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ArepitaComponent } from './arepita.component';
import { ArepitaDetailComponent } from './arepita-detail.component';
import { ArepitaPopupComponent } from './arepita-dialog.component';
import { ArepitaDeletePopupComponent } from './arepita-delete-dialog.component';

export const arepitaRoute: Routes = [
    {
        path: 'arepita',
        component: ArepitaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Arepitas'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'arepita/:id',
        component: ArepitaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Arepitas'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const arepitaPopupRoute: Routes = [
    {
        path: 'arepita-new',
        component: ArepitaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Arepitas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'arepita/:id/edit',
        component: ArepitaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Arepitas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'arepita/:id/delete',
        component: ArepitaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Arepitas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
