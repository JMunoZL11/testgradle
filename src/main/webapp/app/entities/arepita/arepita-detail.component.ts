import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Arepita } from './arepita.model';
import { ArepitaService } from './arepita.service';

@Component({
    selector: 'jhi-arepita-detail',
    templateUrl: './arepita-detail.component.html'
})
export class ArepitaDetailComponent implements OnInit, OnDestroy {

    arepita: Arepita;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private arepitaService: ArepitaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInArepitas();
    }

    load(id) {
        this.arepitaService.find(id)
            .subscribe((arepitaResponse: HttpResponse<Arepita>) => {
                this.arepita = arepitaResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInArepitas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'arepitaListModification',
            (response) => this.load(this.arepita.id)
        );
    }
}
