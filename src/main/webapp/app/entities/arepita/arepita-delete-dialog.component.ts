import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Arepita } from './arepita.model';
import { ArepitaPopupService } from './arepita-popup.service';
import { ArepitaService } from './arepita.service';

@Component({
    selector: 'jhi-arepita-delete-dialog',
    templateUrl: './arepita-delete-dialog.component.html'
})
export class ArepitaDeleteDialogComponent {

    arepita: Arepita;

    constructor(
        private arepitaService: ArepitaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.arepitaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'arepitaListModification',
                content: 'Deleted an arepita'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-arepita-delete-popup',
    template: ''
})
export class ArepitaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private arepitaPopupService: ArepitaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.arepitaPopupService
                .open(ArepitaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
